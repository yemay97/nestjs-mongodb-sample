import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Customer } from './interfaces/customer.interface';
import { CreateCustomerDTO } from './dto/create-customer.dto';

@Injectable()
export class CustomerService {
  constructor(
    // used the @InjectModel method to inject the Customer model into the CustomerService class.
    // seamlessly carry out several database related activities such as, creating a customer
    @InjectModel('Customer') private readonly customerModel: Model<Customer>,
  ) {}
  // fetch all customers
  async getAllCustomer(): Promise<Customer[]> {
    const customers = await this.customerModel.find().exec();
    return customers;
  }
  // Get a single customer
  async getCustomer(customerID): Promise<Customer> {
    const customer = await this.customerModel.findById(customerID).exec();
    return customer;
  }
  // post a single customer
  async addCustomer(createCustomerDTO: CreateCustomerDTO): Promise<Customer> {
    const newCustomer = await new this.customerModel(createCustomerDTO);
    return newCustomer.save();
  }

  // Edit customer details
  async updateCustomer(
    customerID,
    createCustomerDTO: CreateCustomerDTO,
  ): Promise<Customer> {
    const updatedCustomer = await this.customerModel.findByIdAndUpdate(
      customerID,
      createCustomerDTO,
      { new: true },
    );
    return updatedCustomer;
  }

  // Delete a customer
  async deleteCustomer(customerID): Promise<any> {
    const deletedCustomer = await this.customerModel.findByIdAndRemove(
      customerID,
    );
    return deletedCustomer;
  }

   // filter customers by name,email,phone,address, minDate~ & ~maxDate
   async filterCustomer(filters): Promise<Customer[]> {
    const customers = await this.customerModel
      .find({
        $and: [
          { email: new RegExp(filters.email) },
          { phone: new RegExp(filters.phone) },
          { address: new RegExp(filters.address) },
          // gte - greater, lte- lesser
          {createdAt: { $gte: filters.minDate, $lte: filters.maxDate }},
        ],
        $or: [{ firstName: new RegExp(filters.customerName, 'i') },
        { lastName: new RegExp(filters.customerName, 'i') }],
      })
      .exec();
    return customers;
  }

  async countCustomer(filters): Promise<any> {
    const results = await this.customerModel
    .find({
      $and: [
        { email: new RegExp(filters.email) },
        { phone: new RegExp(filters.phone) },
        { address: new RegExp(filters.address) },
        // gte - greater, lte- lesser
        {createdAt: { $gte: filters.minDate, $lte: filters.maxDate }},
      ],
      $or: [{ firstName: new RegExp(filters.customerName, 'i') },
      { lastName: new RegExp(filters.customerName, 'i') }],
    }).count()
    .exec();
    return results;
  }
}
