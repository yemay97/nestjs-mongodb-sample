import {
  Controller,
  Get,
  Res,
  HttpStatus,
  Post,
  Body,
  Put,
  Query,
  NotFoundException,
  Delete,
  Param,
} from '@nestjs/common';
import { CustomerService } from './customer.service';
import { CreateCustomerDTO } from './dto/create-customer.dto';

@Controller('customer')
export class CustomerController {
  // In order to interact with the database, the CustomerService was injected into the controller
  constructor(private customerService: CustomerService) {}

  // add a customer
  @Post()
  async addCustomer(@Res() res, @Body() createCustomerDTO: CreateCustomerDTO) {
    const customer = await this.customerService.addCustomer(createCustomerDTO);
    return res.status(HttpStatus.OK).json({
      message: 'Customer has been created successfully',
      customer,
    });
  }

  // Retrieve customers list
  @Get()
  async getAllCustomer(@Res() res) {
    const customers = await this.customerService.getAllCustomer();
    return res.status(HttpStatus.OK).json(customers);
  }

  // filter by passing in body
  @Get('/get-filter-customer')
  async getFilterCutomer(@Res() res, @Body() filters: any) {
    const customers = await this.customerService.filterCustomer(filters);
    if (!customers) {
      throw new NotFoundException('No Customer Found with the filters!');
    }
    return res.status(HttpStatus.OK).json(customers);
  }

  // filter by passing in body
  @Get('/get-count-customer')
  async getCountCustomer(@Res() res, @Body() criterias: any) {
    const results = await this.customerService.countCustomer(criterias);
    if (!results) {
      throw new NotFoundException('No results found with the criteria!');
    }
    return res.status(HttpStatus.OK).json({count: results});
  }

  // Fetch a particular customer using ID
  @Get(':customerID')
  async getCustomer(@Res() res, @Param('customerID') customerID) {
    const customer = await this.customerService.getCustomer(customerID);
    if (!customer) {
      throw new NotFoundException('Customer does not exist!');
    }
    return res.status(HttpStatus.OK).json(customer);
  }

  // Update a customer's details
  @Put('/update')
  async updateCustomer(
    @Res() res,
    @Query('customerID') customerID,
    @Body() createCustomerDTO: CreateCustomerDTO,
  ) {
    const customer = await this.customerService.updateCustomer(
      customerID,
      createCustomerDTO,
    );
    if (!customer) {
      throw new NotFoundException('Customer does not exist!');
    }
    return res.status(HttpStatus.OK).json({
      message: 'Customer has been successfully updated',
      customer,
    });
  }

  // Delete a customer
  @Delete('/delete')
  async deleteCustomer(@Res() res, @Query('customerID') customerID) {
    const customer = await this.customerService.deleteCustomer(customerID);
    if (!customer) {
      throw new NotFoundException('Customer does not exist');
    }
    return res.status(HttpStatus.OK).json({
      message: 'Customer has been deleted',
      customer,
    });
  }

}
