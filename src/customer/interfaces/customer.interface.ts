import { Document } from 'mongoose';
// used for type-checking and to determine the type of values that will be received by the application

export interface Customer extends Document {
  readonly firstName: string;
  readonly lastName: string;
  readonly email: string;
  readonly phone: string;
  readonly address: string;
  readonly description: string;
  readonly createdAt: Date;
}
