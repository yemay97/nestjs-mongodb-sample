import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CustomerModule } from './customer/customer.module';
import { uri } from './env';

@Module({
  imports: [
    // Mongoose module for MongoDB uses the forRoot() method to supply the connection to the database.
    MongooseModule.forRoot(
      uri,
      // 'mongodb://localhost/customer-app',
      {
        useNewUrlParser: true,
      },
    ),
    CustomerModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
