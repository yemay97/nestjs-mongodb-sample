"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.CustomerSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    email: String,
    phone: String,
    address: String,
    description: String,
    createdAt: { type: Date, default: Date.now },
});
//# sourceMappingURL=customer.schema.js.map