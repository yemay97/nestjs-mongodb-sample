"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
let CustomerService = class CustomerService {
    constructor(customerModel) {
        this.customerModel = customerModel;
    }
    async getAllCustomer() {
        const customers = await this.customerModel.find().exec();
        return customers;
    }
    async getCustomer(customerID) {
        const customer = await this.customerModel.findById(customerID).exec();
        return customer;
    }
    async addCustomer(createCustomerDTO) {
        const newCustomer = await new this.customerModel(createCustomerDTO);
        return newCustomer.save();
    }
    async updateCustomer(customerID, createCustomerDTO) {
        const updatedCustomer = await this.customerModel.findByIdAndUpdate(customerID, createCustomerDTO, { new: true });
        return updatedCustomer;
    }
    async deleteCustomer(customerID) {
        const deletedCustomer = await this.customerModel.findByIdAndRemove(customerID);
        return deletedCustomer;
    }
    async filterCustomer(filters) {
        const customers = await this.customerModel
            .find({
            $and: [
                { email: new RegExp(filters.email) },
                { phone: new RegExp(filters.phone) },
                { address: new RegExp(filters.address) },
                { createdAt: { $gte: filters.minDate, $lte: filters.maxDate } },
            ],
            $or: [{ firstName: new RegExp(filters.customerName, 'i') },
                { lastName: new RegExp(filters.customerName, 'i') }],
        })
            .exec();
        return customers;
    }
    async countCustomer(filters) {
        const results = await this.customerModel
            .find({
            $and: [
                { email: new RegExp(filters.email) },
                { phone: new RegExp(filters.phone) },
                { address: new RegExp(filters.address) },
                { createdAt: { $gte: filters.minDate, $lte: filters.maxDate } },
            ],
            $or: [{ firstName: new RegExp(filters.customerName, 'i') },
                { lastName: new RegExp(filters.customerName, 'i') }],
        }).count()
            .exec();
        return results;
    }
};
CustomerService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('Customer')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], CustomerService);
exports.CustomerService = CustomerService;
//# sourceMappingURL=customer.service.js.map